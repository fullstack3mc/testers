package tempUtil;

import org.junit.platform.commons.util.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.Arrays;

class TemporarySolutionToReadBugsAndPutIntoDML {

    public static void main(String[] args) {
        try {
        InputStream is = new ClassPathResource("/tempData/bugs").getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while (reader.ready()) {
                String line = reader.readLine();
                if(StringUtils.isNotBlank(line)) {
                    String[] splitted = line.replace("\"", "").split(",");
                    String insert = getInsert(splitted);
                    System.out.println(insert);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private static String getInsert(String[] splitted) {
        return String.format("        <insert tableName=\"bug\">\n" +
                "            <column name=\"bug_id\" value=\"%s\"/>\n" +
                "            <column name=\"device_id\" value=\"%s\"/>\n" +
                "            <column name=\"tester_id\" value=\"%s\"/>\n" +
                "        </insert>", splitted[0], splitted[1], splitted[2]);
    }

}