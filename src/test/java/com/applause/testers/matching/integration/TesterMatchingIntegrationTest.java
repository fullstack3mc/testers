package com.applause.testers.matching.integration;

import com.applause.testers.TestersApplication;
import com.applause.testers.matching.domain.dto.MatchTestersPayload;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = TestersApplication.class)
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = LiquibaseAutoConfiguration.class)
public class TesterMatchingIntegrationTest {

    public static final String I_PHONE_4 = "iPhone 4";
    public static final String I_PHONE_5 = "iPhone 5";
    public static final String I_PHONE_6 = "iPhone 6";
    public static final String DEVICE_ALL = "ALL";
    public static final String COUNTRY_US = "US";
    public static final String COUNTRY_JP = "JP";
    public static final String COUNTRY_ALL = "ALL";

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldReturnUserMarkFirstWith2BugsCountedWhenSearchingIphone4ForUS()
            throws Exception {

        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setCountries(Collections.singletonList(COUNTRY_US));
        dto.setDevices(Collections.singletonList(I_PHONE_4));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].testerId", is(1)))
                .andExpect(jsonPath("$[0].countedBugs", is(2)))
                .andExpect(jsonPath("$[0].name", is("User Mark")));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldReturnUserJohnFirstAndMarkSecondWhenSearchingAllDevicesForUS()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setCountries(Collections.singletonList(COUNTRY_US));
        dto.setDevices(Collections.singletonList(DEVICE_ALL));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].testerId", is(2)))
                .andExpect(jsonPath("$[1].testerId", is(1)));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldReturnUserJohnFirstAndLeeSecondWhenSearchingAllIphone6ForAllCountries()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setCountries(Collections.singletonList(COUNTRY_ALL));
        dto.setDevices(Collections.singletonList(I_PHONE_6));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].testerId", is(2)))
                .andExpect(jsonPath("$[0].name", is("User John")))
                .andExpect(jsonPath("$[0].countedBugs", is(3)))
                .andExpect(jsonPath("$[1].testerId", is(3)))
                .andExpect(jsonPath("$[1].name", is("User Lee")))
                .andExpect(jsonPath("$[1].countedBugs", is(1)));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldReturnUserLeeFirstForAllCountriesAndAllDevices()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setCountries(Collections.singletonList(COUNTRY_ALL));
        dto.setDevices(Collections.singletonList(DEVICE_ALL));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].testerId", is(3)))
                .andExpect(jsonPath("$[0].name", is("User Lee")))
                .andExpect(jsonPath("$[0].countedBugs", is(6)));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldRespondWith400WhenNoCriteriaFound()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("No criteria found"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldRespondWith400WhenNoCountriesFound()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setDevices(Collections.singletonList(DEVICE_ALL));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("No countries in criteria found"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldRespondWith400WhenNoDevicesFound()
            throws Exception {
        MatchTestersPayload dto = new MatchTestersPayload();
        dto.setCountries(Collections.singletonList(COUNTRY_ALL));
        mvc.perform(post("/match").content(asJsonString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("No devices in criteria found"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}