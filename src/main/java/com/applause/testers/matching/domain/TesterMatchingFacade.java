package com.applause.testers.matching.domain;

import com.applause.testers.matching.domain.dto.MatchTestersPayload;
import com.applause.testers.matching.domain.dto.MatchTestersResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class TesterMatchingFacade {

    private final TesterMatchingService testerMatchingService;

    public List<MatchTestersResponse> matchTesters(MatchTestersPayload payload) {
        return testerMatchingService.matchTesters(payload);
    }
}
