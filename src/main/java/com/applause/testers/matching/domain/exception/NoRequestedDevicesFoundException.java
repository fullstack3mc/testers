package com.applause.testers.matching.domain.exception;

public class NoRequestedDevicesFoundException extends RuntimeException{

    public NoRequestedDevicesFoundException(){
        super("No devices in criteria found");
    }
}
