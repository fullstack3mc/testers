package com.applause.testers.matching.domain;

import com.applause.testers.matching.domain.dto.MatchTestersPayload;
import com.applause.testers.matching.domain.dto.MatchTestersResponse;
import com.applause.testers.matching.domain.dto.TesterInfoResponse;
import com.applause.testers.matching.domain.entity.QBug;
import com.applause.testers.matching.domain.validator.TesterMatchingValidator;
import com.applause.testers.matching.repository.QDevice;
import com.applause.testers.matching.repository.QTester;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

@AllArgsConstructor
@Component
public class TesterMatchingService {

    private static final QBug qBug = QBug.bug;
    private static final QTester qTester = QTester.tester;

    private final EntityManager em;
    private final List<TesterMatchingValidator> validators;

    public List<MatchTestersResponse> matchTesters(MatchTestersPayload payload) {
        validators.forEach(v -> v.validate(payload));
        JPAQueryFactory factory = new JPAQueryFactory(em);
        NumberPath<Long> aliasCount = Expressions.numberPath(Long.class, "countedBugs");
        List<MatchTestersResponse> testersMatching = factory.from(qBug)
                .select(Projections.constructor(MatchTestersResponse.class,
                        qBug.tester.id.as("testerId"),
                        qBug.count().as(aliasCount)
                ))
                .where(getSearchPredicateForMatchTesters(payload))
                .groupBy(qBug.tester.id)
                .orderBy(aliasCount.desc())
                .fetch();
        attachTesterNames(testersMatching);
        return testersMatching;
    }

    private void attachTesterNames(List<MatchTestersResponse> testersMatching) {
        List<Long> testerIds = testersMatching.stream()
                .map(MatchTestersResponse::getTesterId)
                .collect(Collectors.toList());
        JPAQueryFactory factory = new JPAQueryFactory(em);
        List<TesterInfoResponse> testers = factory.from(qTester)
                .select(Projections.constructor(TesterInfoResponse.class, qTester.id, qTester.firstName, qTester.lastName))
                .where(qTester.id.in(testerIds))
                .fetch();
        Map<Long, String> testerInfoMap= new HashMap<>();
        testers.forEach(t -> testerInfoMap.put(t.getId(), t.getFirstName() + " " + t.getLastName()));
        testersMatching.forEach(tm -> tm.setName(testerInfoMap.get(tm.getTesterId())));
    }

    private Predicate getSearchPredicateForMatchTesters(MatchTestersPayload payload) {
        QDevice qDevice = QDevice.device;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (!isEmpty(payload.getCountries()) && payload.getCountries().stream().noneMatch(c -> c.equalsIgnoreCase("ALL"))) {
            booleanBuilder.and(qTester.country.in(payload.getCountries()));
        }
        if (!isEmpty(payload.getDevices()) && payload.getDevices().stream().noneMatch(d -> d.equalsIgnoreCase("ALL"))) {
            booleanBuilder.and(qDevice.description.in(payload.getDevices()));
        }
        return booleanBuilder.getValue();
    }
}
