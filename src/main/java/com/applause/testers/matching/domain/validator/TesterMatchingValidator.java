package com.applause.testers.matching.domain.validator;

import com.applause.testers.matching.domain.dto.MatchTestersPayload;

public interface TesterMatchingValidator {

    void validate(MatchTestersPayload payload);
}
