package com.applause.testers.matching.domain.exception;

public class NoRequestedCountriesFoundException extends RuntimeException{

    public NoRequestedCountriesFoundException(){
        super("No countries in criteria found");
    }
}
