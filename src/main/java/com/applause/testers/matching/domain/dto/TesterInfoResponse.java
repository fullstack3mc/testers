package com.applause.testers.matching.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TesterInfoResponse {

    private final Long id;
    private final String firstName;
    private final String lastName;
}
