package com.applause.testers.matching.domain.entity;

import com.applause.testers.matching.repository.Device;
import com.applause.testers.matching.repository.Tester;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "bug")
@Data
public class Bug {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bug_id")
    private Long bugId;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;

    @ManyToOne
    @JoinColumn(name = "tester_id")
    private Tester tester;
}
