package com.applause.testers.matching.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class MatchTestersPayload {

    private List<String> countries;
    private List<String> devices;
}
