package com.applause.testers.matching.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = { Exception.class })
    public ResponseEntity<String> handleRuntimeException(Exception exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(value = { NoRequestedDevicesFoundException.class })
    public ResponseEntity<String> handleNoRequestedDevicesFound(NoRequestedDevicesFoundException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { NoCriteriaFoundException.class })
    public ResponseEntity<String> handleNoCriteriaFound(NoCriteriaFoundException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { NoRequestedCountriesFoundException.class })
    public ResponseEntity<String> handleNoRequestedCountriesFound(NoRequestedCountriesFoundException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
}