package com.applause.testers.matching.domain.exception;

public class NoCriteriaFoundException extends RuntimeException{

    public NoCriteriaFoundException(){
        super("No criteria found");
    }
}
