package com.applause.testers.matching.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class MatchTestersResponse {

    private final Long testerId;

    private final Long countedBugs;

    private String name;

    public MatchTestersResponse(Long testerId, Long countedBugs) {
        this.testerId = testerId;
        this.countedBugs = countedBugs;
    }

    public void setName(String name) {
        this.name = name;
    }
}
