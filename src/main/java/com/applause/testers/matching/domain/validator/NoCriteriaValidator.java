package com.applause.testers.matching.domain.validator;

import com.applause.testers.matching.domain.dto.MatchTestersPayload;
import com.applause.testers.matching.domain.exception.NoCriteriaFoundException;
import com.applause.testers.matching.domain.exception.NoRequestedCountriesFoundException;
import com.applause.testers.matching.domain.exception.NoRequestedDevicesFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class NoCriteriaValidator implements TesterMatchingValidator{

    @Override
    public void validate(MatchTestersPayload payload) {
        if(CollectionUtils.isEmpty(payload.getCountries()) && CollectionUtils.isEmpty(payload.getDevices())){
            throw new NoCriteriaFoundException();
        } else if(CollectionUtils.isEmpty(payload.getCountries())){
            throw new NoRequestedCountriesFoundException();
        } else if (CollectionUtils.isEmpty(payload.getDevices())){
            throw new NoRequestedDevicesFoundException();
        }
    }
}