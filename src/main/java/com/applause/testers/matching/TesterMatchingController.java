package com.applause.testers.matching;

import com.applause.testers.matching.domain.TesterMatchingFacade;
import com.applause.testers.matching.domain.dto.MatchTestersPayload;
import com.applause.testers.matching.domain.dto.MatchTestersResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class TesterMatchingController {

    private final TesterMatchingFacade facade;

    @PostMapping("match")
    public List<MatchTestersResponse> matchTesters(@RequestBody MatchTestersPayload payload){
        return facade.matchTesters(payload);
    }
}
