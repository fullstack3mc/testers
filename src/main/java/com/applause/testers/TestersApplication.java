package com.applause.testers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class TestersApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestersApplication.class, args);
	}

}
