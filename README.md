# README #

Implementation of matching bugs per testers

### Created by ###

 Michał Michniewicz

### Set up ###

* Install docker desktop
* Install Jdk11 & set environment variables
* Install Maven & set environment variables
* Clone repository from:
      https://bitbucket.org/fullstack3mc/testers/src/develop/
* Run in terminal command: 
  docker run --rm --name pg-docker -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres
* In project location run: 
  mvn clean install -DskipTests (because of integration tests)
* Check Integration test:
  com.applause.testers.matching.integration.TesterMatchingIntegrationTest
* Run App:
  com.applause.testers.TestersApplication

### Test application manually ###

* HTTP method: POST
* Url: http://localhost:8999/match
* Basic Authentication. 
  User: 'applause'
  Password: 'password'

### Used technologies ###
* Docker
* Java 11
* Postgres
* Liquibase
* JPA, Hibernate
* Spring Boot
* Rest

### Used technologies in tests ###
* MockMvc
* H2