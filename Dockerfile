FROM openjdk:11-jre-slim-buster
ARG JAR_FILE=target/testers-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} testers.jar
EXPOSE 8092
ENTRYPOINT ["java", "-Xmx500m","-jar","/testers.jar"]